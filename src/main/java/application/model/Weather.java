package application.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "WEATHER")
public class Weather{

    @Id
    @GeneratedValue
    private int id;
    private double temperature;
    private double humidity;
    private String city;
    private Date date;
}
