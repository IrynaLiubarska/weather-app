package application.service;

import application.database.WeatherCrudRepository;
import application.model.Weather;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.util.Optional;

@Service
public class WeatherDataService {
    
    @Autowired
    WeatherCrudRepository weatherCrudRepository;
    
    public Optional<Weather> findByCityAndDate(String city, Data date){
        return weatherCrudRepository.findByCityAndDate(city, date);
    }
}
