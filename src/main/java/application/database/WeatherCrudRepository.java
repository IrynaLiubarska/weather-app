package application.database;

import application.model.Weather;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.xml.crypto.Data;
import java.util.Optional;

@Repository
public interface WeatherCrudRepository extends CrudRepository<Weather, Long> {

    Optional<Weather> findByCityAndDate(String city, Data date);
}
